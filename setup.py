#!/usr/bin/env python3

import os
import sys
import shutil
from pathlib import Path
import subprocess
import urllib.request
import shutil

INSTALLER = os.path.dirname(os.path.realpath(__file__))

#TODO: Find all links to the relevant architectures and put them into a datastructes that fits our purpose
TOOLS = {
    'x86_64': {
        'fzf': '',
        'fd': ''
    }
}


def download(source: str, target: str):
    with urllib.request.urlopen(source) as response, open(target, 'wb') as out_file:
        data = response.read()
        out_file.write(data)


def install_tools():
    pass


def panic(err):
    print('\n\tPANIC: ' + err)
    sys.exit(1)


def main():
    if os.getuid() is not 0:
        panic("Run this script with root privilage")

    os.chdir(INSTALLER)
    name = 'baumarkt'

    bindir = Path('/usr/local/bin')
    files = Path('./files')

    print('Installing {what}'.format(what=name))
    os.makedirs(bindir, exist_ok=True)

    for file in files.glob('*'):
        dest = bindir / file.name
        if dest.exists():
            os.remove(dest)
        src = file.resolve()
        print(str(src) + '->' + str(dest))
        shutil.copy2(str(src), str(dest))

    print('{what} successfully installed'.format(what=name))


main()
