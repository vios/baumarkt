
function fail() {
    msgbox "Failure" $1
    exit 1
}

function dirbox()
{
    local -n ref=$1
    local title=$2
    local start=$3
    exec 3>&1;
    result=$(dialog --backtitle "$MAIN_TITLE" --title "$title" --dselect "$start" 30 80 2>&1 1>&3)
    exitcode=$?;
    exec 3>&-;
    if [[ $exitcode -ne 0 ]]; then
        fail "Directory box error ($exitcode)"
    fi
    clear
    ref="$result"
}

function msgbox()
{
    dialog --backtitle "$MAIN_TITLE" --title "$1" --msgbox "$2" 0 0
    clear
}

function ask()
{
    dialog --backtitle "$MAIN_TITLE" --title "$1" --yesno "$2" 0 0
    local ret=$?
    clear
    return $ret
}

function inputbox()
{
    local -n ref=$1
    local title=$2
    exec 3>&1;
    result=$(dialog --backtitle "$MAIN_TITLE" --title "$title" --inputbox '' 0 0 2>&1 1>&3);
    exitcode=$?;
    exec 3>&-;
    if [[ $exitcode -ne 0 ]]; then
        fail "Inputbox error ($exitcode)"
    fi
    clear
    ref="$result"
}
