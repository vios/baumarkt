#!/usr/bin/env python3

"""
Makes sure that the given application is only started once by script
"""

import sys
import os
import time
import random
import string
from tempfile import gettempdir
from pathlib import Path
from pprint import pformat

#TODO: Come on. Use something better for debugging like the debug module in Python.
DBGMODE = False

class Note(object):
    home = Path(gettempdir()) / 'py-ncom'

    """For lock files or temp messages for when you don't own stdout or stderr."""
    @staticmethod
    def path(**kwargs) -> Path:
        if not Note.home.exists():
            Note.home.mkdir()
        name = time.strftime('%Y-%m-%dT%H:%M:%SZ',time.localtime(None)) + gen_id()
        ext = "pymsg"
        if 'name' in kwargs:
            name = str(kwargs.get('name'))
        if 'ext' in kwargs:
            ext = str(kwargs.get('ext'))
        extension = '.' + ext
        return Note.home / Path(name + extension)

    @staticmethod
    def write(content, **kwargs):
        path = Note.path(**kwargs)
        with(open(path,'w')) as file:
            file.write(content)
        


def check_or_deny(pid_path):
    if DBGMODE: Note.write(f'looking for pid file: {pformat(pid_path)}')
    if os.path.exists(pid_path):
        with open(pid_path) as lock:
            suspect = lock.read()
            if pid_exists(int(suspect)):
                if DBGMODE: Note.write('exec denied')
                sys.exit(0)
            if DBGMODE: Note.write('dead process found. pid file removed.')
        os.remove(pid_path)
    return


def grant_exec(request, app):
    pid = os.fork()

    if pid == 0:
        os.execvpe(app, request, os.environ)
        if DBGMODE: Note.write(f'exec granted for {pformat(request)}')
        return
    Note.write(str(pid), ext='pypid', name=app)


def pid_exists(pid):        
    """ Check For the existence of a unix pid. """
    try:
        os.kill(pid, 0)
    except OSError:
        return False
    else:
        return True


def main():
    if len(sys.argv) < 3:
        raise RuntimeError("You executed me not correctly! The syntax is watchpup.py [APPLICATION] [DELAY] (arg1) (arg2)...")

    delay = int(sys.argv[1])
    app = str(sys.argv[2])
    request = sys.argv[2:]
    if DBGMODE: Note.write(f'exec request "{app}" with delay: {delay}')
    time.sleep(delay)
    if DBGMODE: Note.write(f'Delay expired')
    pid_path = Note.path(ext='pypid', name=app)
    check_or_deny(pid_path)
    grant_exec(request, app)



def gen_id(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choices(chars, k=size))


if __name__ == "__main__":
    try:
        if DBGMODE: Note.write('STARTED IN DEBUG MODE',name='puppy',ext='pydbg')
        main()
    except Exception as ex:
        import traceback
        Note.write(pformat(traceback.format_exception(type(ex),ex,ex.__traceback__)), ext='pycrash')
        
