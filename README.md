# Baumarkt

A collection of Bash and almost self contained Python 3 scripts for various tasks. You will find here a script to do a quick git upload, cleanup all dead symlinks in a folder, list all groups on your system, convert html to markdown... I think you get the idea. 

For example `tarpipe`  in particular is a very useful command that moved files from A to B fast while preserving all ACLs.

*Note: Most commands do not have a help page or man page. Documentation is lacking at the moment and I will fix that when things calm down.*

**Also remember I wrote those for use on my systems. My code might eat your goldfish so be careful if you use something here.**

I also tend to delete scripts sometimes if I feel like they are kinda silly. If you are missing something just go back a few commits.

## Dependencies

Depends on what you want to use. Most depdendencies are either included or get downloaded in the process

Make sure you have [fzf](https://https://github.com/junegunn/fzf) because I use that for a few tools [Node.js](https://github.com/creationix/nvm) and [yarn](https://yarnpkg.com/en/) might also not be a bad idea if you want to use the web tools.

The guys and girls who want to use the Project Launcher (`proj`) need [rofi](https://github.com/DaveDavenport/rofi).

And if you don't already have `xclip` shame on you and install it with your local package manager.

## Setup

Run `setup.sh` and all command should be available for the current user. Updates are handeled with git pulls only. If there are new commands you have to run `setup.sh` again.

## TODO

- Use [skim](https://github.com/lotabout/skim) instead of fzf

<sub>I also added the great sh.py script from amoffat because some scripts depend on it... Buy him a beer if you see him or check out his github page: https://github.com/amoffat/sh</sub>
